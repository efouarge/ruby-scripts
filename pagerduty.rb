# This job is defined to use the hotness widget reference: https://gist.github.com/runar/8927307
 

require 'net/https'

http = Net::HTTP.new('cision.pagerduty.com', 443)
http.use_ssl = true
http.start do |http|

   # define the schedule that you want the widget to update
SCHEDULER.every '5m', first_in: 0 do |job|

   # you can set the status to acknowledged, triggered, resolve
   req = Net::HTTP::Get.new('/api/v1/incidents?status=resolved')

   # we make an HTTP basic auth by passing the
   # username and password from pagerduty
   req.basic_auth 'username', 'password'


   resp, data = http.request(req)

   # Uncomment to see the output from the API request
   #print resp.body

   json = JSON.parse(response.body)
   
   # send the data to the dashboard widget, make sure the request is the same event that you have enabled.
   #send_event(:pagerduty_triggered, value: json['total'])
   #send_event(:pagerduty_acknowledged, value: json['total'])
   send_event(:pagerduty_resolved, value: json['total'])

end

